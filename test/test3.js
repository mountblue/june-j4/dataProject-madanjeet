let expect = require("chai").expect;
let path = require("path");
let csvMatchFile = path.resolve("CSVfiles/matches (copy).csv");
let csvDeliveriesFile = path.resolve("CSVfiles/deliveries (copy).csv");
let fileThree = path.resolve("questions/quest3.js");
let quest3 = require(fileThree);
describe("test-quest3", function () {
    it("should return the match_id of all the matches played in the year 2016", function (done) {
        const expectedResult = [1, 2, 3];
        quest3.getMatchId(csvMatchFile).then(function (ids) {
            try {
                expect(ids).deep.equals(expectedResult);
                done();
            } catch (e) {
                done(e);
            }
        })
    })
    // it("should return bowling_teams (for the given match_ids)/(year 2016)", async function () {
    //     const expectedResult = ["Royal Challengers Bangalore", "Gujarat Lions"];
    //     await quest3.getBowlingTeams(csvMatchFile, csvDeliveriesFile).then(function (ids) {
    //         expect(ids).deep.equals(expectedResult);
    //     })
    // })
    it("should  return the object containing distinct bowling teams & extra_runs per delivery of the bowling_teams of the year 2016", async function () {
        const expectedResult = {
            bowlingTeamArr: ['Royal Challengers Bangalore', 'Gujarat Lions'],
            extraRunsPerDelivery: [{
                    team: 'Royal Challengers Bangalore',
                    extra: '5'
                },
                {
                    team: 'Royal Challengers Bangalore',
                    extra: '3'
                },
                {
                    team: 'Gujarat Lions',
                    extra: '1'
                },
                {
                    team: 'Gujarat Lions',
                    extra: '0'
                },
                {
                    team: 'Gujarat Lions',
                    extra: '0'
                },
                {
                    team: 'Gujarat Lions',
                    extra: '0'
                },
                {
                    team: 'Gujarat Lions',
                    extra: '0'
                },
                {
                    team: 'Gujarat Lions',
                    extra: '1'
                },
                {
                    team: 'Gujarat Lions',
                    extra: '0'
                },
                {
                    team: 'Gujarat Lions',
                    extra: '1'
                },
                {
                    team: 'Gujarat Lions',
                    extra: '0'
                },
                {
                    team: 'Gujarat Lions',
                    extra: '2'
                },
                {
                    team: 'Gujarat Lions',
                    extra: '0'
                }
            ]
        }
        await quest3.getExtraRunsPerDelivery(csvMatchFile, csvDeliveriesFile).then(function (ids) {
            expect(ids).deep.equals(expectedResult);
        })
    })
    it("should  return the extra_runs per team of the year 2016", async function () {
        const expectedResult = [{
                y: 8,
                label: "Royal Challengers Bangalore"
            },
            {
                y: 5,
                label: "Gujarat Lions"
            }
        ];
        await quest3.getExtraRunsPerTeam(csvMatchFile, csvDeliveriesFile).then(function (ids) {
            expect(ids).deep.equals(expectedResult);
        })
    })
})