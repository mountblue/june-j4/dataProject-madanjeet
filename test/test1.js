let expect = require("chai").expect;
let path = require("path");
let csvMatchFile = path.resolve("CSVfiles/matches (copy).csv");
let fileOne = path.resolve("questions/quest1.js");
let quest1 = require(fileOne);
describe("test-quest1", function () {
    it("should return season of each match of ipl", async function () {
        const expectedResult = ["2016", "2016", "2016", "2009", "2009", "2009", "2015", "2015", "2015", "2014", "2014", "2014"];
        const yearArr = quest1.getEachMatchSeason(csvMatchFile);
        await yearArr.then(function (result) {
            expect(result).deep.equals(expectedResult);
        })
    })
    it("should return total number of matches played per year ", async function () {
        let expectedResult = [{
                label: '2009',
                y: 3
            },
            {
                label: '2014',
                y: 3
            },
            {
                label: '2015',
                y: 3
            },
            {
                label: '2016',
                y: 3
            }
        ];
        let result = await quest1.getTotalMatchesPerYear(csvMatchFile);
        expect(result).deep.equal(expectedResult);
    })
})