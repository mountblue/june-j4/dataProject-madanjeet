let expect = require("chai").expect;
let path = require("path");
let csvMatchFile = path.resolve("CSVfiles/matches (copy).csv");
let fileFive = path.resolve("questions/quest5.js");
let quest5 = require(fileFive);
describe("test-quest5", function () {

    it("should return all distinct cities where matches of ipl were played", async function () {
        const expectedResult = { cityArr:
            [ 'Hyderabad',
              'Pune',
              'Rajkot',
              'Indore',
              'Bangalore',
              'Hyderabad',
              'Mumbai',
              'Indore',
              'Pune',
              'Mumbai',
              'Mumbai',
              'Hyderabad' ],
           distinctCityArr: [ 'Hyderabad', 'Pune', 'Rajkot', 'Indore', 'Bangalore', 'Mumbai' ] }
        const distinctCityArr = quest5.getDistinctCity(csvMatchFile);
        await distinctCityArr.then(function (result) {
            expect(result).deep.equals(expectedResult);
        })
    })

    it("should return total matches played in distinct cities", async function () {
        const expectedResult = [{
                label: 'Hyderabad',
                y: 3
            },
            {
                label: 'Mumbai',
                y: 3
            },
            {
                label: 'Pune',
                y: 2
            },
            {
                label: 'Indore',
                y: 2
            },
            {
                label: 'Rajkot',
                y: 1
            },
            {
                label: 'Bangalore',
                y: 1
            }
        ];
        const distinctCityArr = quest5.getTotalMatchesPlayedInEachCity(csvMatchFile);
        await distinctCityArr.then(function (result) {
            expect(result).deep.equals(expectedResult);
        })
    })
})