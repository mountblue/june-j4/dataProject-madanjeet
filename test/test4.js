let expect = require("chai").expect;
let path = require("path");
let csvMatchFile = path.resolve("CSVfiles/matches (copy).csv");
let csvDeliveriesFile = path.resolve("CSVfiles/deliveries (copy).csv");
let fileFour = path.resolve("questions/quest4.js");
let quest4 = require(fileFour);
describe("test-quest4", function () {
    it("should return the match_id of all the matches played in the year 2015", function (done) {
        const expectedResult = [7, 8, 9];
        quest4.getMatchId(csvMatchFile).then(function (ids) {
            try {
                expect(ids).deep.equals(expectedResult);
                done();
            } catch (e) {
                done(e);
            }
        })
    })
    it("should return object containing all bowlers & runs per delivery for 2015", async function () {
        const expectedResult = { bowlersArr: [], runsPerDelivery: [] };
        await quest4.getRunsPerDelivery(csvMatchFile, csvDeliveriesFile).then(function (ids) {
            expect(ids).deep.equals(expectedResult);
        })
    })
    it("should return total runs given by each bowler in 2015)", async function () {
        const expectedResult = [];
        await quest4.getTotalRunsPerBowler(csvMatchFile, csvDeliveriesFile).then(function (ids) {
            expect(ids).deep.equals(expectedResult);
        })
    })
    it("should return economy of each bowler of 2015", async function () {
        const expectedResult = [];
        await quest4.getEconomy(csvMatchFile, csvDeliveriesFile).then(function (ids) {
            expect(ids).deep.equals(expectedResult);
        })
    })
})