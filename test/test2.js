let expect = require("chai").expect;
let path = require("path");
let csvMatchFile = path.resolve("CSVfiles/matches (copy).csv");
let fileTwo = path.resolve("questions/quest2.js");
let quest2 = require(fileTwo);
describe("test-quest2", function () {
    it("should return object containing (array of all teams) and (array of objects for each line of data containing(season & winner))",
        async function () {
            const expectedResult = {
                winnerPerMatchArr: [{
                        season: '2016',
                        winner: 'Royal Challengers Bangalore'
                    },
                    {
                        season: '2016',
                        winner: 'Royal Challengers Bangalore'
                    },
                    {
                        season: '2016',
                        winner: 'Kolkata Knight Riders'
                    },
                    {
                        season: '2009',
                        winner: 'Kings XI Punjab'
                    },
                    {
                        season: '2009',
                        winner: 'Royal Challengers Bangalore'
                    },
                    {
                        season: '2009',
                        winner: 'Sunrisers Hyderabad'
                    },
                    {
                        season: '2015',
                        winner: 'Royal Challengers Bangalore'
                    },
                    {
                        season: '2015',
                        winner: 'Kings XI Punjab'
                    },
                    {
                        season: '2015',
                        winner: 'Royal Challengers Bangalore'
                    },
                    {
                        season: '2014',
                        winner: 'Royal Challengers Bangalore'
                    },
                    {
                        season: '2014',
                        winner: 'Sunrisers Hyderabad'
                    },
                    {
                        season: '2014',
                        winner: 'Royal Challengers Bangalore'
                    }
                ],
                teamArr: ['Royal Challengers Bangalore',
                    'Kolkata Knight Riders',
                    'Kings XI Punjab',
                    'Sunrisers Hyderabad'
                ]
            }
            await quest2.matchWonByEachTeam(csvMatchFile).then(function (result) {
                expect(result).deep.equals(expectedResult);
            })
        })
    it("should return (array of all teams) and (array of objects for each line of data containing(season, no.of matches, winner))", async function () {
        const expectedResult = {
            winnerPerMatchPerSeasonArr: [{
                    winner: 'Royal Challengers Bangalore',
                    numberOfMatches: 2,
                    year: '2016'
                },
                {
                    winner: 'Royal Challengers Bangalore',
                    numberOfMatches: 1,
                    year: '2009'
                },
                {
                    winner: 'Royal Challengers Bangalore',
                    numberOfMatches: 2,
                    year: '2015'
                },
                {
                    winner: 'Royal Challengers Bangalore',
                    numberOfMatches: 2,
                    year: '2014'
                },
                {
                    winner: 'Kolkata Knight Riders',
                    numberOfMatches: 1,
                    year: '2016'
                },
                {
                    winner: 'Kolkata Knight Riders',
                    numberOfMatches: 0,
                    year: '2009'
                },
                {
                    winner: 'Kolkata Knight Riders',
                    numberOfMatches: 0,
                    year: '2015'
                },
                {
                    winner: 'Kolkata Knight Riders',
                    numberOfMatches: 0,
                    year: '2014'
                },
                {
                    winner: 'Kings XI Punjab',
                    numberOfMatches: 0,
                    year: '2016'
                },
                {
                    winner: 'Kings XI Punjab',
                    numberOfMatches: 1,
                    year: '2009'
                },
                {
                    winner: 'Kings XI Punjab',
                    numberOfMatches: 1,
                    year: '2015'
                },
                {
                    winner: 'Kings XI Punjab',
                    numberOfMatches: 0,
                    year: '2014'
                },
                {
                    winner: 'Sunrisers Hyderabad',
                    numberOfMatches: 0,
                    year: '2016'
                },
                {
                    winner: 'Sunrisers Hyderabad',
                    numberOfMatches: 1,
                    year: '2009'
                },
                {
                    winner: 'Sunrisers Hyderabad',
                    numberOfMatches: 0,
                    year: '2015'
                },
                {
                    winner: 'Sunrisers Hyderabad',
                    numberOfMatches: 1,
                    year: '2014'
                }
            ],
            teamArr: ['Royal Challengers Bangalore',
                'Kolkata Knight Riders',
                'Kings XI Punjab',
                'Sunrisers Hyderabad'
            ]
        }
        let result = await quest2.getmatchesWonByEachTeamPerYear(csvMatchFile)
        expect(result).deep.equals(expectedResult);
    })
    it("should return the format-converted-data for highcharts", async function () {
        const expectedResult = [{
                name: 'Royal Challengers Bangalore',
                data: [2, 1, 2, 2]
            },
            {
                name: 'Kolkata Knight Riders',
                data: [1, 0, 0, 0]
            },
            {
                name: 'Kings XI Punjab',
                data: [0, 1, 1, 0]
            },
            {
                name: 'Sunrisers Hyderabad',
                data: [0, 1, 0, 1]
            }
        ];
        let result = await quest2.dataFormatConverter(csvMatchFile)
        expect(result).deep.equals(expectedResult);
    })
})