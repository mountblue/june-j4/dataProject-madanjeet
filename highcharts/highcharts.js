function loadDocument(filePath) {
    var xhttp = new XMLHttpRequest();
    let fileContent = [];
    xhttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            fileContent = JSON.parse(this.responseText);
        }
    };
    xhttp.open("GET", filePath, false);
    xhttp.send();
    return fileContent;
}

function graphOfMatchPerYear() {
    let matchPerYrDataSet = loadDocument("../JSONfile/quest1.json");
    var chart = new CanvasJS.Chart("chartContainer", {
        animationEnabled: true,
        title: {
            text: "Number of matches played in different seasons"
        },
        axisX: {
            title: "Seasons"
        },
        axisY: {
            title: "Number of matches",
            titleFontColor: "#4F81BC",
            lineColor: "#4F81BC",
            labelFontColor: "#4F81BC",
            tickColor: "#4F81BC"
        },
        legend: {
            cursor: "pointer",
            itemclick: toggleDataSeries
        },
        data: [{
            type: "spline",
            name: "Match per year",
            showInLegend: true,
            yValueFormatString: "#,##0.# Matches",
            dataPoints: matchPerYrDataSet
        }]
    });
    chart.render();

    function toggleDataSeries(e) {
        if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
        } else {
            e.dataSeries.visible = true;
        }
        e.chart.render();
    }
}
graphOfMatchPerYear();

function quest2() {
    let winsPerYrPerTeamDataSet = loadDocument("../JSONfile/quest2.json");
    Highcharts.chart('chartContainer2', {
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Matches won per team per season'
        },
        xAxis: {
            title: {
                text: 'Years'
            },
            categories: ['2017', '2008', '2009', '2010', '2011', '2012', '2013', '2014', '2015', '2016']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Matches won per team per season'
            }
        },
        legend: {
            reversed: true
        },
        plotOptions: {
            series: {
                stacking: 'normal'
            }
        },
        series: winsPerYrPerTeamDataSet
    });
}
quest2();

function drawGraphOfQuest3() {
    let extrasPerTeam = loadDocument("../JSONfile/quest3.json");
    var chart = new CanvasJS.Chart("chartContainer3", {
        animationEnabled: true,
        title: {
            text: "Extra runs per team in season 2016"
        },
        axisX: {
            title: "Teams"
        },
        axisY: {
            title: "Total extras",
            titleFontColor: "#4F81BC",
            lineColor: "#4F81BC",
            labelFontColor: "#4F81BC",
            tickColor: "#4F81BC"
        },
        legend: {
            cursor: "pointer",
            itemclick: toggleDataSeries
        },
        data: [{
            type: "column", //line,pyramid,column,pie,
            name: "Extras per team",
            showInLegend: false,
            yValueFormatString: "#,##0.# Runs",
            dataPoints: extrasPerTeam
        }]
    });
    chart.render();

    function toggleDataSeries(e) {
        if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
        } else {
            e.dataSeries.visible = true;
        }
        e.chart.render();
    }
}
drawGraphOfQuest3();

function drawGraphOfQuest4() {
    let topTenEconomicalBowlers = loadDocument("../JSONfile/quest4.json");
    var chart = new CanvasJS.Chart("chartContainer4", {
        animationEnabled: true,
        title: {
            text: "Top 10 economical bowlers Of 2015"
        },
        axisX: {
            title: "Bowlers"
        },
        axisY: {
            title: "Economy",
            titleFontColor: "#4F81BC",
            lineColor: "#4F81BC",
            labelFontColor: "#4F81BC",
            tickColor: "#4F81BC"
        },
        legend: {
            cursor: "pointer",
            itemclick: toggleDataSeries
        },
        data: [{
            type: "column",
            name: "Top 10 economical bowlers",
            showInLegend: true,
            yValueFormatString: "Bowling economy: #,##0.#",
            dataPoints: topTenEconomicalBowlers
        }]
    });
    chart.render();

    function toggleDataSeries(e) {
        if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
        } else {
            e.dataSeries.visible = true;
        }
        e.chart.render();
    }
}
drawGraphOfQuest4();

function drawGraphOfQuest5() {
    let topTenEconomicalBowlers = loadDocument("../JSONfile/quest5.json");
    var chart = new CanvasJS.Chart("chartContainer5", {
        animationEnabled: true,
        title: {
            text: "Maximum matches played in 10 different cities"
        },
        axisX: {
            title: "Cities"
        },
        axisY: {
            title: "No. of matches",
            titleFontColor: "#4F81BC",
            lineColor: "#4F81BC",
            labelFontColor: "#4F81BC",
            tickColor: "#4F81BC"
        },
        legend: {
            cursor: "pointer",
            itemclick: toggleDataSeries
        },
        data: [{
            type: "column",
            name: "Maximum matches played in 10 distinct cities",
            showInLegend: true,
            yValueFormatString: "#,##0.# matches",

            dataPoints: topTenEconomicalBowlers
        }]
    });
    chart.render();

    function toggleDataSeries(e) {
        if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
        } else {
            e.dataSeries.visible = true;
        }
        e.chart.render();
    }
}
drawGraphOfQuest5();