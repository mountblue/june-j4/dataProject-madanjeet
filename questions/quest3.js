const fs = require("fs");
const csv = require("fast-csv");
const path = require("path");
const matchFile = path.resolve("../CSVfiles/matches.csv");
const deliveryFile = path.resolve("../CSVfiles/deliveries.csv");

let getMatchId = function (matchFile) {
    return new Promise(function (resolve, reject) {

        let ids = [];

        fs.createReadStream(matchFile)
            .pipe(csv())
            .on("data", function (data) {
                if (data[1] === "2016") {
                    ids.push(Number(data[0]));
                }
            })
            .on("end", function () {
                resolve(ids);
            })
    })
}

function getExtraRunsPerDelivery(matchFile, deliveryFile) {
    return new Promise(function (resolve, reject) {
        var ids = getMatchId(matchFile);
        ids.then(function (ids) {
            let bowlingTeamArr = [];
            let extraRunsPerDelivery = [];
            fs.createReadStream(deliveryFile).pipe(csv())
                .on('data', function (data) {
                    let id = data[0];
                    if (ids.includes(Number(id))) {
                        let team = data[3];
                        if (!bowlingTeamArr.includes(team)) {
                            bowlingTeamArr.push(team);
                        }
                        let teamWithExtrasObj = {
                            team: team,
                            extra: data[16]
                        }
                        extraRunsPerDelivery.push(teamWithExtrasObj);
                    }
                }).on('end', function () {
                    let extrasObj={
                        bowlingTeamArr:bowlingTeamArr,
                        extraRunsPerDelivery:extraRunsPerDelivery
                    }
                    resolve(extrasObj);
                })
        });
    })
}

function getExtraRunsPerTeam(matchFile, deliveryFile) {
    return new Promise(function (resolve, reject) {
        let extraRunsPerTeamArr = [];
        let extrasObj = getExtraRunsPerDelivery(matchFile, deliveryFile);
            extrasObj.then(function (extrasObj) {
                let extraRunsPerDelivery=extrasObj["extraRunsPerDelivery"];
                let bowlingTeamArr=extrasObj["bowlingTeamArr"];
                for (let j = 0; j < bowlingTeamArr.length; j++) {
                    count = 0;
                    let obj = {};
                    for (let i = 0; i < extraRunsPerDelivery.length; i++) {
                        if (bowlingTeamArr[j] == extraRunsPerDelivery[i].team) {
                            count += Number(extraRunsPerDelivery[i].extra);
                        }
                    }
                    obj["label"] = bowlingTeamArr[j];
                    obj["y"] = count;
                    extraRunsPerTeamArr.push(obj);
                }
                // require("fs").writeFile("../JSONfile/quest3.json", JSON.stringify(extraRunsPerTeamArr, null, 10), () => {
                //     console.log("file created");
                // })
                resolve(extraRunsPerTeamArr);
            });
        })
}
// getExtraRunsPerTeam(matchFile, deliveryFile);

module.exports = {
    getMatchId: getMatchId,
    getExtraRunsPerDelivery: getExtraRunsPerDelivery,
    getExtraRunsPerTeam: getExtraRunsPerTeam
}