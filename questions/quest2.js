const path = require("path");
const matchFile = path.resolve("../CSVfiles/matches.csv");

function matchWonByEachTeam(matchFile) {
    let fs = require("fs");
    let csv = require("fast-csv");
    return new Promise(function (resolve, reject) {
        let winnerPerMatchArr = [];
        let teamArr = [];
        fs.createReadStream(matchFile)
            .pipe(csv())
            .on("data", function (data) {
                let team = data[10];
                if (data[10] !== "" && data[10] !== "winner") {
                   let winnerPerMatchObj = {
                        season: data[1],
                        winner: data[10]
                    }
                    winnerPerMatchArr.push(winnerPerMatchObj);
                }
                if (!teamArr.includes(team)) {
                    if (team !== "" && team !== "winner") {
                        teamArr.push(team);
                    }
                }
            })
            .on("end", function () {
                let result = {
                    winnerPerMatchArr:winnerPerMatchArr,
                    teamArr:teamArr
                }
                resolve(result);
            });
    })
}

function getmatchesWonByEachTeamPerYear(matchFile) {
    return new Promise(async function (resolve, reject) {
        let matchObj = matchWonByEachTeam(matchFile);
        matchObj.then(async function (matchObj) {
                let arr = matchObj["winnerPerMatchArr"];
                let teamArr = matchObj["teamArr"];
                let count = 0;
                let winnerPerMatchPerSeasonobj = {};
                let winnerPerMatchPerSeasonArr = [];
                for (let j = 0; j < teamArr.length; j++) {
                    count = 0;
                    for (let i = 0; i < arr.length; i++) {
                        if (i === arr.length - 1) {
                            if (teamArr[j] === arr[i].winner) {
                                ++count;
                                winnerPerMatchPerSeasonobj = {
                                    winner: teamArr[j],
                                    numberOfMatches: count,
                                    year: arr[i].season
                                }
                                winnerPerMatchPerSeasonArr.push(winnerPerMatchPerSeasonobj);
                                count = 0;
                            } else {
                                winnerPerMatchPerSeasonobj = {
                                    winner: teamArr[j],
                                    numberOfMatches: count,
                                    year: arr[i].season
                                }
                                winnerPerMatchPerSeasonArr.push(winnerPerMatchPerSeasonobj);
                                count = 0;
                            }
                        } else if (arr[i].season === arr[i + 1].season) {
                            if (teamArr[j] === arr[i].winner) {
                                ++count;
                            }
                        } else {
                            if (count > 0) {
                                if (teamArr[j] === arr[i].winner) {
                                    ++count;
                                    winnerPerMatchPerSeasonobj = {
                                        winner: teamArr[j],
                                        numberOfMatches: count,
                                        year: arr[i].season
                                    }
                                    winnerPerMatchPerSeasonArr.push(winnerPerMatchPerSeasonobj);
                                    count = 0;
                                } else {
                                    winnerPerMatchPerSeasonobj = {
                                        winner: teamArr[j],
                                        numberOfMatches: count,
                                        year: arr[i].season
                                    }
                                    winnerPerMatchPerSeasonArr.push(winnerPerMatchPerSeasonobj);
                                    count = 0;
                                }
                            } else {
                                if (teamArr[j] === arr[i].winner) {
                                    ++count;
                                    winnerPerMatchPerSeasonobj = {
                                        winner: teamArr[j],
                                        numberOfMatches: count,
                                        year: arr[i].season
                                    }
                                    winnerPerMatchPerSeasonArr.push(winnerPerMatchPerSeasonobj);
                                    count = 0;
                                } else {
                                    winnerPerMatchPerSeasonobj = {
                                        winner: teamArr[j],
                                        numberOfMatches: count,
                                        year: arr[i].season
                                    }
                                    winnerPerMatchPerSeasonArr.push(winnerPerMatchPerSeasonobj);
                                }
                            }
                        }
                    }
                }
                let resultObj = {
                    winnerPerMatchPerSeasonArr:winnerPerMatchPerSeasonArr,
                    teamArr:teamArr
                }
                resolve(resultObj);
            })
        })
}

function dataFormatConverter(matchFile) {
    return new Promise(async function (resolve, reject) {
        let dataObj = getmatchesWonByEachTeamPerYear(matchFile);
            dataObj.then(function (dataObj) {
                let data = dataObj["winnerPerMatchPerSeasonArr"];
                let teamArr = dataObj["teamArr"];

                let dataArr = [];
                let result = [];
                for (let i = 0; i < teamArr.length; i++) {
                    let obj = {};
                    for (let j = 0; j < data.length; j++) {
                        if (teamArr[i] == data[j].winner) {
                            dataArr.push(data[j].numberOfMatches);
                        }
                    }
                    obj = {
                        name: teamArr[i],
                        data: dataArr
                    }
                    dataArr = [];
                    result.push(obj);
                }
                // require("fs").writeFile("../JSONfile/quest2.json", JSON.stringify(result, null, 10), () => {
                //     console.log("file created");
                // })
                resolve(result);
            })
        })
}
// dataFormatConverter(matchFile);
module.exports = {
    matchWonByEachTeam: matchWonByEachTeam,
    getmatchesWonByEachTeamPerYear: getmatchesWonByEachTeamPerYear,
    dataFormatConverter: dataFormatConverter
}