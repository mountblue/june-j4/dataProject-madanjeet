const path = require("path");
const matchFile = path.resolve("../CSVfiles/matches.csv");

function getEachMatchSeason(matchFile) {
    let fs = require("fs");
    let csv = require("fast-csv");
    return new Promise(function (resolve, reject) {
        let yearArr = [];
        fs.createReadStream(matchFile)
            .pipe(csv())
            .on("data", function (data) {
                if (Number(data[1])) {
                    yearArr.push(data[1]);
                }
            })
            .on("end", function () {
                resolve(yearArr);
            })
    })
}

function getTotalMatchesPerYear(matchFile) {
    return new Promise(function (resolve, reject) {
        const year = getEachMatchSeason(matchFile);
        year.then(async function (year) {
            let yearArr = await year;

            let count = 0;
            let matchPerYrArr = [];
            for (let i = 0; i < yearArr.length; i++) {
                if (i === yearArr.length - 1) {
                    if (count > 0) {
                        ++count;
                    } else {
                        count = 1;
                    }
                } else if (yearArr[i] === yearArr[i + 1]) {
                    ++count;
                } else {
                    let obj = {};
                    obj["label"] = yearArr[i];
                    obj["y"] = ++count;
                    matchPerYrArr.push(obj);
                    count = 0;
                }
            }
            let obj = {};
            obj["label"] = yearArr[yearArr.length - 1];
            obj["y"] = count;
            matchPerYrArr.push(obj);
            var result = matchPerYrArr;
            result.sort(function(a,b){
                return a.label-b.label;
            })
            // require("fs").writeFile("../JSONfile/quest1.json", JSON.stringify(result, null, 10), () => {
            //     console.log("file created");
            // })
            resolve(result);
        })
    })
}
// getTotalMatchesPerYear(matchFile);
module.exports = {
    getTotalMatchesPerYear: getTotalMatchesPerYear,
    getEachMatchSeason: getEachMatchSeason
}