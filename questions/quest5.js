const path = require("path");
const matchFile = path.resolve("../CSVfiles/matches.csv");

function getDistinctCity(matchFile) {
    let fs = require("fs");
    let csv = require("fast-csv");
    return new Promise(function (resolve, reject) {
        let distinctCityArr = [];
        let cityArr = [];
        fs.createReadStream(matchFile)
            .pipe(csv())
            .on("data", function (data) {
                if (data[2] !== "city")
                    cityArr.push(data[2]);
                let city = data[2];
                if (!distinctCityArr.includes(city)) {
                    if (city !== "" && city !== "city") {
                        distinctCityArr.push(city);
                    }
                }
            })
            .on("end", function () {
                let cityObj = {
                    cityArr: cityArr,
                    distinctCityArr: distinctCityArr
                }
                // console.log(cityObj);
                resolve(cityObj);
            });
    })
}

function getTotalMatchesPlayedInEachCity(matchFile) {
    return new Promise(function (resolve, reject) {
        const cityObj=getDistinctCity(matchFile);
            cityObj.then(async function (cityObj) {
                let cityArr = await cityObj["cityArr"];
                let distinctCity=await cityObj["distinctCityArr"];
                let count = 0;
                let matchPerCityArr = [];
                for (let i = 0; i < distinctCity.length; i++) {
                    count = 0;
                    for (let j = 0; j < cityArr.length; j++) {
                        if (distinctCity[i] == cityArr[j]) {
                            ++count;
                        }
                    }
                    let obj = {};
                    obj["label"] = distinctCity[i];
                    obj["y"] = count;
                    matchPerCityArr.push(obj);
                }
                matchPerCityArr.sort(function(a,b){
                    return b.y-a.y;
                })
                let result=matchPerCityArr.splice(0,10);
                require("fs").writeFile("../JSONfile/quest5.json", JSON.stringify(result, null, 10), () => {
                    console.log("file created");
                })
                resolve(result);
            })
        })
}
// getTotalMatchesPlayedInEachCity(matchFile);
module.exports = {
    getTotalMatchesPlayedInEachCity: getTotalMatchesPlayedInEachCity,
    getDistinctCity: getDistinctCity
}