const fs = require("fs");
const csv = require("fast-csv");
const path = require("path");
const matchFile = path.resolve("../CSVfiles/matches.csv");
const deliveryFile = path.resolve("../CSVfiles/deliveries.csv");
let getMatchId = function (matchFile) {
    return new Promise(function (resolve, reject) {
        let ids = [];
        fs.createReadStream(matchFile)
            .pipe(csv())
            .on("data", function (data) {
                if (data[1] === "2015") {
                    ids.push(Number(data[0]));
                }
            })
            .on("end", function () {
                resolve(ids);
            })
    })
}

function getRunsPerDelivery(matchFile, deliveryFile) {
    return new Promise(function (resolve, reject) {
        var ids = getMatchId(matchFile);
        ids.then(function (ids) {
            let bowlersArr = [];
            let runsPerDelivery = [];
            fs.createReadStream(deliveryFile).pipe(csv())
                .on('data', function (data) {
                    let id = data[0];
                    if (ids.includes(Number(id))) {
                        let bowler = data[8];
                        if (!bowlersArr.includes(bowler)) {
                            bowlersArr.push(bowler);
                        }
                        let runsPerDeliveryObj = {
                            bowler: bowler,
                            run: data[17],
                            wide: data[10],
                            noball: data[13]
                        }
                        runsPerDelivery.push(runsPerDeliveryObj);
                    }
                }).on('end', function () {
                    let runsPerDeliveryPerBowlerObj={
                        bowlersArr:bowlersArr,
                        runsPerDelivery:runsPerDelivery
                    }
                    resolve(runsPerDeliveryPerBowlerObj);
                })
        });
    })
}

function getTotalRunsPerBowler(matchFile, deliveryFile) {
    return new Promise(function (resolve, reject) {
        let totalRunsPerBowlerArr = [];
        let runsPerDeliveryPerBowlerObj = getRunsPerDelivery(matchFile, deliveryFile);
            runsPerDeliveryPerBowlerObj.then(function (runsPerDeliveryPerBowlerObj) {
                let bowlersArr=runsPerDeliveryPerBowlerObj["bowlersArr"];
                let runsPerDelivery=runsPerDeliveryPerBowlerObj["runsPerDelivery"];
                for (let j = 0; j < bowlersArr.length; j++) {
                    count = 0;
                    counter = 0;
                    let obj = {};
                    for (let i = 0; i < runsPerDelivery.length; i++) {
                        if (bowlersArr[j] == runsPerDelivery[i].bowler) {
                            count += Number(runsPerDelivery[i].run);
                            if (runsPerDelivery[i].wide == 0 && runsPerDelivery[i].noball == 0) {
                                ++counter;
                            }
                        }
                    }
                    obj["bowler"] = bowlersArr[j];
                    obj["totalRuns"] = count;
                    obj["totalDeliveries"] = counter;
                    totalRunsPerBowlerArr.push(obj);
                }
                resolve(totalRunsPerBowlerArr);
            });
        })
}

function getEconomy(matchFile, deliveryFile) {
    return new Promise(function (resolve, reject) {
        let economyPerBowlerArr = [];
        let obj = {};
        let totalRunsPerBowler = getTotalRunsPerBowler(matchFile, deliveryFile);
        totalRunsPerBowler.then(function (totalRunsPerBowler) {
            for (let i = 0; i < totalRunsPerBowler.length; i++) {
                obj = {
                    label: totalRunsPerBowler[i].bowler,
                    y: (totalRunsPerBowler[i].totalRuns / totalRunsPerBowler[i].totalDeliveries) * 6
                }
                economyPerBowlerArr.push(obj);
            }
            economyPerBowlerArr.sort(function (a, b) {
                return a.y - b.y;
            });
            let topTen = economyPerBowlerArr.splice(0, 10);

            // require("fs").writeFile("../JSONfile/quest4.json", JSON.stringify(topTen, null, 10), () => {
            //     console.log("file created");
            // })
            resolve(topTen);
        })

    })

}
// getEconomy(matchFile, deliveryFile);


module.exports = {
    getMatchId: getMatchId,
    getRunsPerDelivery: getRunsPerDelivery,
    getTotalRunsPerBowler: getTotalRunsPerBowler,
    getEconomy: getEconomy
}